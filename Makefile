.PHONY: all build html

all: build html

build:
	@docker build -t regreb/markup .

%.html: %.md
	@docker run -v $(PWD):/markup/host regreb/markup make $(@)

html:
	@docker run -v $(PWD):/markup/host regreb/markup make all

clean:
	@rm -f ./README.html
