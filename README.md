# GitHub markup

This is the source repo for the [regreb/markup](https://hub.docker.com/r/regreb/markup/) docker image.

## Build

```sh
make build
```

## Sample usage with local files

```sh
make html
```
